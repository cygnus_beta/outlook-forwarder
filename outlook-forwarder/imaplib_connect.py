#!/usr/bin/env python
# -*- coding: utf-8 -*-

import imaplib
import configparser
import logging
import os

from logger import logger


def connect(verbose: bool = False):
    if verbose:
        logger.setLevel(logging.DEBUG)

    # Read the config file
    config = configparser.ConfigParser()
    config.read([os.path.abspath('settings.ini')])

    # Connect to the server
    hostname = config.get('imap_server', 'hostname')
    if verbose:
        print('Connecting to', hostname)
    c = imaplib.IMAP4_SSL(hostname)
    if verbose:
        c.set_debuglevel(1)

    # Login to our account
    username = config.get('imap_account', 'username')
    password = config.get('imap_account', 'password')
    if verbose:
        print('Logging in as', username)
    c.login(username, password)

    return c


if __name__ == '__main__':
    c = connect(verbose=True)
    try:
        logger.info(c)
    finally:
        c.logout()
        logger.info("logged out")
