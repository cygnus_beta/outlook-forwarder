#!/usr/bin/env python
# -*- coding: utf-8 -*-

import configparser
import logging
import os
import smtplib

from logger import logger


def connect(verbose: bool = False):
    if verbose:
        logger.setLevel(logging.DEBUG)

    # Read the config file
    config = configparser.ConfigParser()
    config.read([os.path.abspath('settings.ini')])

    # Connect to the server
    hostname = config.get('smtp_server', 'hostname')
    if verbose:
        print('Connecting to', hostname)

    c = smtplib.SMTP(hostname, 587)
    if verbose:
        c.set_debuglevel(1)

    # identify ourselves to smtp client
    c.ehlo()
    # secure our email with tls encryption
    c.starttls()
    # re-identify ourselves as an encrypted connection
    c.ehlo()

    # Login to our account
    username = config.get('smtp_account', 'username')
    password = config.get('smtp_account', 'password')
    if verbose:
        print('Logging in as', username)
    c.login(username, password)

    return c


if __name__ == '__main__':
    c = connect(verbose=True)
    try:
        logger.info(c)
    finally:
        c.quit()
        logger.info("closed SMTP connection")
