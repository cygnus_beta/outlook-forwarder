#!/usr/bin/env python
# -*- coding: utf-8 -*-

import imaplib
import itertools
import os
import time

from logger import logger


def save_incoming_mail(raw_full_mail):
    mail_file_path = ''
    for i in itertools.count():
        mail_file_path = os.path.join('assets_for_testing',
                                      'my_mail_test_{}.txt'.format(i))
        if not os.path.exists(mail_file_path):
            break

    logger.info('>>> saving mail as "{}"...'
                .format(os.path.basename(mail_file_path)))

    with open(mail_file_path, 'w', newline='\n') as f:
        f.write(raw_full_mail)


if __name__ == '__main__':
    from main import imap_session

    imaplib.Debug = 4
    session = imap_session(test_saving_only=True)
    session.start()
