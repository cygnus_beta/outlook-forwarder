#!/usr/bin/env python
# -*- coding: utf-8 -*-

import configparser
import logging
import os
import time

from apscheduler.schedulers.background import BackgroundScheduler

import smtplib_connect
from logger import logger
from mail_parser import parse
from mail_util import message_info, get_latest_saved_mail_file_path, \
    get_latest_saved_raw_full_mail
from outgoing_msg_maker import make_outgoing_msg
from smtplib_sendmail import send_email

# initialize scheduler
scheduler = BackgroundScheduler({
    'apscheduler.timezone': 'Asia/Tokyo',
    'apscheduler.executors.default': {
        'class': 'apscheduler.executors.pool:ThreadPoolExecutor',
        'max_workers': '1'
    }
})
scheduler.start()


def add_queue(raw_full_mail: str, verbose: bool = False):
    # noinspection PyTypeChecker
    job = scheduler.add_job(run, args=[raw_full_mail, verbose],
                            misfire_grace_time=None)
    if verbose:
        logger.info('>>> job details: {}'.format(job))


def run(raw_full_mail: str, verbose: bool = False):
    if verbose:
        logger.setLevel(logging.DEBUG)

    logger.info('>>> parsing email...')
    incoming_msg = parse(raw_full_mail)
    logger.info('>>> sub: "{}"'.format(incoming_msg.sub))

    outgoing_msg = make_outgoing_msg(incoming_msg)
    if verbose:
        print(outgoing_msg)

    c = smtplib_connect.connect(verbose)
    try:
        logger.info('>>> forwarding mail...')
        send_email(c, outgoing_msg, verbose)
        logger.info('>>> successfully forwarded!')
    except Exception as e:
        raise Exception(e)
    finally:
        logger.info(">>> closing smtp connection...")
        c.quit()


if __name__ == '__main__':
    logger.setLevel(logging.DEBUG)

    # Parse latest saved raw mail file.
    mail_file_path = get_latest_saved_mail_file_path()
    raw_full_mail = get_latest_saved_raw_full_mail()

    logger.info('>>> use saved latest mail "{}"...'
                .format(os.path.basename(mail_file_path)))
    add_queue(raw_full_mail, verbose=True)
    time.sleep(3600)  # keep python running during processing queue
