#!/usr/bin/env python
# -*- coding: utf-8 -*-

import configparser
import logging
import os
import smtplib
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import smtplib_connect
from logger import logger
from mail_util import message_info


def send_email(c: smtplib.SMTP, outgoing_msg: message_info,
               verbose: bool = False):
    if verbose:
        logger.setLevel(logging.DEBUG)

    mail = MIMEMultipart('alternative')
    mail['From'] = outgoing_msg.from_
    mail['To'] = outgoing_msg.to
    mail['Subject'] = outgoing_msg.sub
    if outgoing_msg.body_text:
        mail.attach(MIMEText(outgoing_msg.body_text, 'plain'))
    if outgoing_msg.body_html:
        mail.attach(MIMEText(outgoing_msg.body_html, 'html'))
    if outgoing_msg.attachments:
        for a in outgoing_msg.attachments:
            mail.attach(a)
    mail['reply-to'] = outgoing_msg.reply_to
    # mail.add_header('reply-to', outgoing_msg.reply_to)
    # mail['reply-to'] = b64_encode(outgoing_msg.reply_to, verbose)

    if verbose:
        print(mail)

    c.sendmail(outgoing_msg.from_, outgoing_msg.to, mail.as_string())


if __name__ == '__main__':
    # Read the config file
    config = configparser.ConfigParser()
    config.read([os.path.abspath('settings.ini')])

    outgoing_msg = message_info()
    outgoing_msg.from_ = config.get('smtp_account', 'username')
    outgoing_msg.to = config.get('forward_to', 'email')
    outgoing_msg.sub = 'test 件名のテスト'
    outgoing_msg.body_text = 'test 本文のテスト'
    outgoing_msg.body_html = (
        '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">\n'
        '<HTML><HEAD>\n'
        '<META content="text/html; charset=gb2312" http-equiv=Content-Type>\n'
        '<META name=GENERATOR content="MSHTML 10.00.9200.17296"></HEAD>\n'
        '<BODY>\n'
        '<P>test&nbsp;本文のテスト</P></BODY></HTML>'
    )

    outgoing_msg.attachments = []
    ImgFilePath = os.path.join('assets_for_testing', 'test.png')
    with open(ImgFilePath, 'rb') as f:
        img_data = f.read()
    a = MIMEApplication(img_data, name=os.path.basename(ImgFilePath))
    a['Content-Disposition'] = 'attachment; filename="{}"'.format(
        os.path.basename(ImgFilePath)
    )
    outgoing_msg.attachments.append(a)

    c = smtplib_connect.connect(verbose=True)
    try:
        send_email(c, outgoing_msg, verbose=True)
        logger.info('>>> successfully sent message!')
    except Exception as e:
        raise Exception(e)
    finally:
        c.quit()
        logger.info("closed SMTP connection")
