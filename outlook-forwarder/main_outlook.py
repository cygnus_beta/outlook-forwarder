#!/usr/bin/env python
# -*- coding: utf-8 -*-

import configparser
import os

from main import imap_session


class settings_handler():
    def outlook_settings_exists(self):
        return os.path.exists('settings-outlook.ini')

    def read_outlook_settings(self):
        # Read the config file
        config = configparser.ConfigParser()
        config.read([os.path.abspath('settings-outlook.ini')])

        self.username = config.get('outlook_account', 'username')
        self.password = config.get('outlook_account', 'password')
        self.forward_to = config.get('forward_to', 'email')

    def copy_to_settings_ini(self):
        # Write the config file
        config = configparser.ConfigParser()

        config['imap_server'] = {'hostname': 'outlook.office365.com'}
        config['imap_account'] = {'username': self.username,
                                  'password': self.password}
        config['smtp_server'] = {'hostname': 'outlook.office365.com'}
        config['smtp_account'] = {'username': self.username,
                                  'password': self.password}
        config['forward_to'] = {'email': self.forward_to}

        settings_ini_path = 'settings.ini'
        with open(settings_ini_path, 'w') as f:
            config.write(f)


if __name__ == '__main__':
    s = settings_handler()
    if not s.outlook_settings_exists():
        print('settings-outlook.ini が見つかりません')
        raise FileNotFoundError()
    s.read_outlook_settings()
    s.copy_to_settings_ini()

    while True:
        session = imap_session()
        session.start()
