#!/usr/bin/env python
# -*- coding: utf-8 -*-

import configparser
import os

from logger import logger
from mail_parser import parse
from mail_util import message_info, get_latest_saved_mail_file_path, \
    get_latest_saved_raw_full_mail


def make_outgoing_msg(incoming_msg: message_info):
    config = configparser.ConfigParser()
    config.read([os.path.abspath('settings.ini')])

    outgoing_msg = message_info()
    outgoing_msg.from_ = config.get('smtp_account', 'username')
    outgoing_msg.to = config.get('forward_to', 'email')
    outgoing_msg.sub = incoming_msg.sub
    outgoing_msg.body_text = incoming_msg.body_text
    outgoing_msg.body_html = incoming_msg.body_html
    outgoing_msg.attachments = incoming_msg.attachments
    if incoming_msg.reply_to:
        outgoing_msg.reply_to = incoming_msg.reply_to
    else:
        outgoing_msg.reply_to = incoming_msg.from_

    return outgoing_msg


if __name__ == '__main__':
    # Parse latest saved raw mail file.
    mail_file_path = get_latest_saved_mail_file_path()
    raw_full_mail = get_latest_saved_raw_full_mail()
    print(raw_full_mail)

    logger.info('>>> parsing saved latest mail "{}"...'
                .format(os.path.basename(mail_file_path)))
    incoming_msg = parse(raw_full_mail, verbose=True)

    outgoing_msg = make_outgoing_msg(incoming_msg)
    print('')
    outgoing_msg.print()
