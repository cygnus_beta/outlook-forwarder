#!/usr/bin/env python
# -*- coding: utf-8 -*-

# main_debug.py
#
# ≒
#
# while true; do
#   python save_incoming_mail.py
#   python queue_handler.py
# done

import logging

from logger import logger
from main import imap_session


def main():
    logger.setLevel(logging.DEBUG)
    while True:
        session = imap_session(test_saving_and_forwarding=True,
                               verbose=True)
        session.start()


if __name__ == '__main__':
    main()
