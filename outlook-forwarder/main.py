#!/usr/bin/env python
# -*- coding: utf-8 -*-
import errno
import imaplib

import imaplib_connect
import queue_handler
from logger import logger
from save_incoming_mail import save_incoming_mail
from socket import error as SocketError


class imap_session:
    def __init__(self, test_saving_only: bool = False,
                 test_saving_and_forwarding: bool = False,
                 verbose: bool = False):
        self.test_saving_only = test_saving_only
        self.test_saving_and_forwarding = test_saving_and_forwarding
        self.verbose = verbose
        self.idle_30_minutes_closed = False
        self.socket_or_session_closed = False

        if self.verbose:
            imaplib.Debug = 4

    def start(self):
        logger.info('>>> logging in...')
        try:
            self.c = imaplib_connect.connect()
        except ConnectionResetError as e:
            logger.warn(e)
            self.socket_or_session_closed = True

        if not self.socket_or_session_closed:
            logger.info('>>> successfully authenticated!')
            try:
                self.c.select(b'INBOX', readonly=True)
                self.start_imap_idle()
            except Exception as e:
                # This error is handled after executing finally clause. See https:/
                # /stackoverflow.com/questions/34249269/finally-and-rethowing-of-ex
                # ception-in-except-raise-in-python for further details.
                raise Exception(e)
            finally:
                if not self.idle_30_minutes_closed and not self.socket_or_session_closed:
                    self.close_and_logout()

    def start_imap_idle(self):
        while True:
            self.incoming_mail_exists = False
            self.idle_30_minutes_closed = False
            self.socket_or_session_closed = False
            logger.info('>>> starting imap idle connection...')
            self.c.send(b'%s IDLE\r\n' % (self.c._new_tag()))
            logger.info('>>> waiting for new mail...')

            try:
                self.start_checking_message()
                if self.idle_30_minutes_closed:
                    return  # Login again immediately
            except SocketError as e:
                if e.errno == errno.ECONNRESET:
                    logger.warn(
                        '>>> socket closed. proceed to restart imap '
                        'session... [Connection reset by peer]')
                    self.socket_or_session_closed = True
                    return  # Login again immediately
                else:
                    raise Exception(e)
            except Exception as e:
                raise Exception(e)
            finally:
                if not self.idle_30_minutes_closed and not self.socket_or_session_closed:
                    logger.info('>>> closing imap idle connection...')
                    self.done()

            if self.incoming_mail_exists:
                logger.info('>>> fetching mail...')
                raw_full_mail = self.fetch_mail()
                logger.info('>>> successfully fetched mail!')

                if self.test_saving_only:
                    save_incoming_mail(raw_full_mail)
                    return

                if self.test_saving_and_forwarding:
                    save_incoming_mail(raw_full_mail)

                logger.info('>>> added mail to queue')
                queue_handler.add_queue(raw_full_mail, self.verbose)

    def start_checking_message(self):
        # running through for 25 minutes
        # t_end = time.time() + 60 * 25
        # while time.time() < t_end:  # <- Not work...
        while True:
            line = self.c.readline().strip()
            if line.startswith(b'* BYE ') or (len(line) == 0):
                logger.info('>>> idle session closed. proceed to restart imap '
                            'session...')
                self.idle_30_minutes_closed = True
                return  # have to login again immediately
            if line.endswith(b'EXISTS'):
                logger.info('>>> NEW MAIL ARRIVED!')
                self.incoming_mail_exists = True
                self.uid = line.split(b' ')[1]
                return  # Proceed to fetch...

    def fetch_mail(self):
        status, datas = self.c.fetch(self.uid, b'(RFC822)')
        raw_full_mail = datas[0][1].decode('utf-8')
        return raw_full_mail

    def done(self):
        try:
            self.c.send(b'DONE\r\n')
        finally:
            pass

    def close_and_logout(self):
        try:
            logger.info('>>> closing...')
            self.c.close()
        finally:
            pass
        self.c.logout()


if __name__ == '__main__':
    while True:
        session = imap_session()
        session.start()
