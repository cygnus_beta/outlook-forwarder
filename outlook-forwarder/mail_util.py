#!/usr/bin/env python
# -*- coding: utf-8 -*-

import base64
import os

from logger import logger


class message_info:
    def __init__(self):
        self.from_ = None
        self.to = None
        self.sub = None
        self.body_text = None
        self.body_html = None
        self.attachments = None
        self.reply_to = None
        # self.cc = None

    def print(self):
        # logger.info(self.__dict__)
        # logger.info(vars(self))
        vars_ = self.__dict__
        for k, v in vars_.items():
            logger.info('{}: {}'.format(k, v))


def get_latest_saved_mail_file_path():
    for i in reversed(range(1000)):
        mail_file_path = os.path.join('assets_for_testing',
                                      'my_mail_test_{}.txt'.format(i))
        if os.path.exists(mail_file_path):
            return mail_file_path


def get_latest_saved_raw_full_mail():
    mail_file_path = get_latest_saved_mail_file_path()

    with open(mail_file_path, 'r') as f:
        raw_full_mail = f.read()

    return raw_full_mail



def b64_encode(s: str, verbose: bool = False):
    s_b64 = base64.b64encode(s.encode('utf-8')).decode('utf-8')
    s_b64_only = s_b64.replace('=', '')
    result = '=?UTF-8?B?{0}?='.format(s_b64_only)
    if verbose:
        print(result)

    return result
