#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import os
from email.mime.application import MIMEApplication

from mailparser import mailparser

from logger import logger
from mail_util import message_info, get_latest_saved_raw_full_mail, \
    get_latest_saved_mail_file_path, b64_encode


def sets_of_name_and_address_to_str(list, b64: bool = False):
    # list of set of str -> str
    list_of_result = []
    for e in list:
        name = e[0]
        address = e[1]
        if name == '':
            list_of_result.append(address)
        else:
            if b64:
                name_b64 = b64_encode(name)
                list_of_result.append('{0} <{1}>'.format(name_b64, address))
            else:
                list_of_result.append('{0} <{1}>'.format(name, address))
    return ','.join(list_of_result)


def parse(raw_full_mail: str, verbose: bool = False):
    if verbose:
        logger.setLevel(logging.DEBUG)

    mail = mailparser.parse_from_string(raw_full_mail)
    if verbose:
        print(mail)

    incoming_msg = message_info()

    if verbose:
        print(mail.from_)  # [('xwfcpggy', 'zyb@sgis.com.cn')]
    incoming_msg.from_ = sets_of_name_and_address_to_str(mail.from_,
                                                         b64=True)

    if verbose:
        print(mail.to)  # [('', 'sales@onlab.cn'), ('', 'sherlock@dada.com')]
    incoming_msg.to = sets_of_name_and_address_to_str(mail.to)

    # unicode
    incoming_msg.sub = mail.subject

    # list of unicode -> unicode
    incoming_msg.body_text = '\r\n\r\n'.join(mail.text_plain)

    # list of unicode -> unicode
    incoming_msg.body_html = '\r\n\r\n'.join(mail.text_html)

    if verbose:
        print(mail.attachments)
    # list
    incoming_msg.attachments = []
    for attachment in mail.attachments:
        data = attachment['payload']
        filename = attachment['filename']
        a = MIMEApplication('', name=filename)
        a.set_payload(data)
        a['Content-Disposition'] = 'attachment; filename="{}"'.format(filename)
        incoming_msg.attachments.append(a)

    incoming_msg.reply_to = sets_of_name_and_address_to_str(mail.reply_to,
                                                            b64=True)
    if verbose:
        incoming_msg.print()

    return incoming_msg


if __name__ == '__main__':
    # Parse latest saved raw mail file.
    mail_file_path = get_latest_saved_mail_file_path()
    raw_full_mail = get_latest_saved_raw_full_mail()

    print(raw_full_mail)

    logger.info('>>> parsing saved latest mail "{}"...'
                .format(os.path.basename(mail_file_path)))
    incoming_msg = parse(raw_full_mail, verbose=True)
