# outlook-forwarder

Outlook に届いたメールを任意のアドレスに自動転送するプログラム。とりあえず今のところは安定していますが、動作保証はありませんのでご注意ください。（β版）

## 機能

- Outlook に IMAP で接続し、メールが来たら、SMTP でメールを転送するというただそれだけの実装です。通常のメーラーアプリのメール取得、送信と同じ仕組みを使っています。

- IMAP IDLE を使ってプッシュ通知を受け取るので、ほぼリアルタイムで転送されます。（遅延は数秒程度）

- 転送するにはどこかの PC かサーバーで、24時間プログラムを動かし続ける必要があります。どこでもいいですが [Heroku](https://www.heroku.com/) だと無料で動かせます。
    - ただしクレジットカードかデビットカードの登録がないと約20日毎に約7日間止まります。
    - Heroku のアカウント登録後、下の「Deploy to Heroku」ボタンからデプロイできます。（未実装）
    
- メールは受信に使用した Outlook のメールアドレスから送信されます。

## 免責事項

- このプログラムは接続先が Outlook のサーバーであればどのような環境でも使用することができます。特定の環境、オフィス等での使用を目的としたものではありません。

- オフィス等で Outlook 公式のメール自動転送機能が無効化されているような環境では、実行する前に必ずネットワーク管理者の許可をとってください。不適切な使用等によって生じた損害等について、開発者は一切の責任を負いません。

- このプログラムは現在β版です。このプログラムの予期せぬ停止、不具合等による利用者の損害等について、開発者は一切の責任を負いません。

- このプログラムが使用するのは Outlook が提供する IMAP、SMTP のエンドポイントのみです。その他のエンドポイントへ接続する機能は有していません。

## Getting Started

### Deploy to Heroku

[To be implemented]

### Host on your own machine

- Prerequisites: Python 3.7.1

1. Clone this repository:

    ```bash
    git clone https://gitlab.com/cygnus_beta/outlook-forwarder
    ```

1. Create an isolated Python environment in an external directory and activate it:

    ```bash
    # Windows
    virtualenv env
    env\Scripts\activate

    # Mac OS / Linux 
    virtualenv env
    source env/bin/activate
    ```

1. Navigate to this repository directory and install dependencies:

    ```bash
    cd outlook-forwarder
    pip install -r requirements.txt
    ```
    
1. Navigate to source directory. Copy `settings-outlook.ini.sample` and rename it to `settings-outlook.ini`, and edit it for your account on your outlook server:

    ```bash
    # Windows   
    cd outlook-forwarder
    copy settings-outlook.ini.sample settings-outlook.ini
 
    # Mac OS / Linux 
    cd outlook-forwarder
    cp settings-outlook.ini.sample settings-outlook.ini
    ```
    
    settings-outlook.ini

    ```
    [outlook_account]
    username: yourmail@yourdomain.com
    password: yoursecretpassword

    [forward_to]
    email: yourmail@yourserver.com
    ```

1. Run the application:

    ```bash
    python3 main_outlook.py
    ```


## License

This repository includes some files and code derived from other softwares or sites. Such files have inherited their original licenses.

- [SpamScope/mail-parser](https://github.com/SpamScope/mail-parser), distributed under the [Apache License Version 2.0](https://github.com/SpamScope/mail-parser/blob/develop/LICENSE.txt)
  
    - ``outlook-forwarder/assets_for_testing/mail_test_9.txt``

- [Stack Overflow](https://stackoverflow.com/questions/18103278/how-to-check-whether-imap-idle-works), distributed under the [Creative Commons Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0)](https://creativecommons.org/licenses/by-sa/3.0/)
    
    - [How to check whether IMAP-IDLE works?](https://stackoverflow.com/questions/18103278/how-to-check-whether-imap-idle-works)

        - ``outlook-forwarder/main.py``
        - ``outlook-forwarder/imaplib_connect.py``
         
         by [Jack Miller](https://stackoverflow.com/users/2484903/jack-miller)

All other files are my writing, MIT License, see [LICENSE.md](LICENSE.md).
