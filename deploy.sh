#!/usr/bin/env bash

set -xeu

bill="$(gcloud beta billing accounts list --format='json')"
echo "${bill}"
bill_num="$(echo "${bill}" | jq length)"
echo "${bill_num}"

if [ "${bill_num}" == "0" ]; then
  set +x
  echo 'エラー：課金が有効になっていません。クレジットカードかデビットカードを登録の上、課金を有効にしてからもう一度お試し下さい。'
  set -x
#   exit 1
fi

id_json="$(gcloud projects list --format='json' --filter='name=outlook-forwarder')"
echo "${id_json}"
id_json_num="$(echo "${id_json}" | jq length)"
echo "${id_json_num}"

if [ "${id_json_num}" == "0" ]; then
  echo '>>> Initializing a project'
  rnd="$(shuf -i 0-999999 -n 1 | xargs printf '%06d')"
  gcloud projects create "outlook-forwarder-${rnd}" --name='outlook-forwarder'
  gcloud projects describe "outlook-forwarder-${rnd}"
  gcloud config set project "outlook-forwarder-${rnd}"
  id="outlook-forwarder-${rnd}"
else
  echo '>>> Configuring a project'
  id="$(gcloud projects list --format='value(projectId)' --filter='name=outlook-forwarder')"
  echo "${id}"
  gcloud config set project "${id}"
fi

git -C outlook-forwarder pull || git clone https://gitlab.com/cygnus_beta/outlook-forwarder.git outlook-forwarder

cd outlook-forwarder
cd outlook-forwarder


if gcloud app describe; then
  :
else
  echo '>>> Creating an App Engine app'
  gcloud app create --region=asia-northeast1
fi

if gcloud services enable cloudbuild.googleapis.com; then
  :
else
  set +x
  echo 'エラー：アカウントで課金は有効になっていますが、このプロジェクト内では有効になっていません。以下の URL からプロジェクトに課金アカウントをリンクしてください。'
  echo "https://console.developers.google.com/billing/linkedaccount?project=${id}"
  set -x
  exit 1
fi

gcloud app deploy app.yaml --project="${id}" --quiet

# gcloud config unset project
